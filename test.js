const {parse} = require('babylon')
const {transform, transformFromAst} = require('babel-core')

const opts = {babelrc: false, sourceMaps: true, minified: true}

const raw = '\nthis'
const ast = parse(raw)

// Fail: transformFromAst --------------------------------

const trAst1 = transformFromAst(ast, raw, Object.assign({inputSourceMap: null}, opts))
console.dir(trAst1.map.mappings) // 'AACA'

const trAst2 = transformFromAst(trAst1.ast, trAst1.code, Object.assign({inputSourceMap: trAst1.map}, opts))
console.dir(trAst2.map.mappings) // ''

// Okay: transform --------------------------------

const trStr1 = transform(raw, Object.assign({inputSourceMap: null}, opts))
console.dir(trStr1.map.mappings) // 'AACA'

const trStr2 = transform(trStr1.code, Object.assign({inputSourceMap: trStr1.map}, opts))
console.dir(trStr2.map.mappings) // 'AACA'
