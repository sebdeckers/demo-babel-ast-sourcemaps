# Babel/Babylon Error Case
Minimal reproducible code sample in [`test.js`](./test.js)

### Run
```
npm start
```

### Result
```
'AACA'
''
'AACA'
'AACA'
```

The second `map.mappings`, using `babel.transformFromAst`, is missing.

The second pass using `babel.transform` is fine, however.
